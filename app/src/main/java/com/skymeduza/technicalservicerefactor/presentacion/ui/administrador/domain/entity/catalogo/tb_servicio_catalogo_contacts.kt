package com.skymeduza.technicalservicerefactor.presentacion.ui.administrador.domain.entity.catalogo

import com.skymeduza.technicalservicerefactor.data.db.base.BaseEntityImpl
import io.objectbox.annotation.Entity

@Entity
data class tb_servicio_catalogo_contacts(
    var id_contact: Long = 0,
    var idCompany: Long = 0,
    var data: String = "",
    var type: String = "",
    var name: String = ""
): BaseEntityImpl()
