package com.skymeduza.technicalservicerefactor.presentacion.ui.splash

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.ImageView
import com.skymeduza.technicalservicerefactor.R
import com.skymeduza.technicalservicerefactor.R.id.iv_imagen_login
import com.skymeduza.technicalservicerefactor.databinding.ActivitySplashBinding
import com.skymeduza.technicalservicerefactor.presentacion.extension.viewBinding
import com.skymeduza.technicalservicerefactor.presentacion.ui.login.LoginActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashActivity : AppCompatActivity() , Runnable {
    private val binding by viewBinding(ActivitySplashBinding::inflate)
    lateinit var  handler: Handler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        simularCarga()


        var imagen : ImageView = findViewById(iv_imagen_login)


        imagen.animate()
            .scaleX(2.0f)
            .scaleY(2.0f)
            .duration = 2700
    }

    private fun simularCarga() {
        handler = Handler()
        handler.postDelayed(this, 3000)
    }

    override fun run(){
        val  sharedPreference =  getSharedPreferences(
            "",
            Context.MODE_PRIVATE
        )
        val usrId = sharedPreference.getString("usr_id","")
        Log.i("ValorDeUsrId", "->" + usrId)
        if(!usrId.equals("")){
            val intent = Intent(this, LoginActivity::class.java)
            intent.putExtra("nombre_usr", ""+usrId)
            startActivity(intent)
            finish()
        }else{
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
    }
}