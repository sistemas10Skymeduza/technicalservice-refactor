package com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.data.service

import com.skymeduza.technicalservicerefactor.data.http.base.BaseApiService
import com.skymeduza.technicalservicerefactor.data.http.base.GenericNetworkResponse
import com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.data.dto.ServiciosResponse
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ServiciosTecAPI : BaseApiService {
    @GET("mantenimientosList")
    suspend fun getMantenimientos(@Query("idUser") idUser:String) : GenericNetworkResponse<ServiciosResponse>
}