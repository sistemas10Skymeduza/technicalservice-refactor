package com.skymeduza.technicalservicerefactor.presentacion.ui.administrador.components.nuevoServicio.fragments.domain.entity

import com.skymeduza.technicalservicerefactor.data.db.base.BaseEntityImpl
import io.objectbox.annotation.Entity

@Entity
data class tb_nuevo_serivicio(
    var contacto: String = "",
    var unidad: String = "",
    var tecnico: String = "",
    var cita_fecha: String = "",
    var cita_hora: String = ""
): BaseEntityImpl()
