package com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.domain.entity.catalogo

import com.skymeduza.technicalservicerefactor.data.db.base.BaseEntityImpl
import io.objectbox.annotation.Entity

@Entity
data class servicioGaleriaGps(
    var id_gps: Long = 0,
    var ruta_imagen: String = "",
    var creacion: String = ""
):BaseEntityImpl()
