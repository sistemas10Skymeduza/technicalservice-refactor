package com.skymeduza.technicalservicerefactor.presentacion.base.util.conectivity

import androidx.lifecycle.MutableLiveData

interface ConnectivityManager {

    val isNetworkAvailable: MutableLiveData<Boolean>

    fun registerConnectionObserver()

    fun unregisterConnectionObserver()
}