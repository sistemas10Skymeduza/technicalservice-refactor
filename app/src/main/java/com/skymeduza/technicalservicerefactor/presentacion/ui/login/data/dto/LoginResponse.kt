package com.skymeduza.technicalservicerefactor.presentacion.ui.login.data.dto

import com.google.gson.annotations.SerializedName

data class LoginResponse(
    @SerializedName("success") var success: Int,
    @SerializedName("data") var data: LoginDataResponse
)