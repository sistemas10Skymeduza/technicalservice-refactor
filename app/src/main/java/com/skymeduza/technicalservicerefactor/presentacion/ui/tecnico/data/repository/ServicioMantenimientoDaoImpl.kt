package com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.data.repository

import com.skymeduza.technicalservicerefactor.data.db.repository.BaseRepositoryImpl
import com.skymeduza.technicalservicerefactor.domain.entity.mantenimiento
import com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.domain.entity.servicios_mantenimiento
import io.objectbox.BoxStore

class ServicioMantenimientoDaoImpl (boxStore: BoxStore) : BaseRepositoryImpl<servicios_mantenimiento>(boxStore, servicios_mantenimiento::class.java), ServicioMantenimientoDao