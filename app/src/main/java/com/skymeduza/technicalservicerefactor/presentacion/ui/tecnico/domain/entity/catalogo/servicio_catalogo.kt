package com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.domain.entity.catalogo

import com.skymeduza.technicalservicerefactor.data.db.base.BaseEntityImpl
import io.objectbox.annotation.Entity

@Entity
data class servicio_catalogo(
    var id_service: Long = 0,
    var reason: String = "",
    var estatus: String = "",
    var mantenimiento: String = ""
): BaseEntityImpl()
