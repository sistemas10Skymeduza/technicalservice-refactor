package com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.domain.mapper

import com.skymeduza.technicalservicerefactor.domain.base.mapper.Mapper
import com.skymeduza.technicalservicerefactor.presentacion.ui.login.data.dto.LoginResponse
import com.skymeduza.technicalservicerefactor.presentacion.ui.login.domain.entity.tb_usuario
import com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.data.dto.ServiciosDataResponse
import com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.data.dto.ServiciosResponse
import com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.domain.entity.servicios_mantenimiento

class ServiciosMapper : Mapper<ServiciosDataResponse, servicios_mantenimiento> {
    override fun mapLeftToRight(obj: ServiciosDataResponse): servicios_mantenimiento = with(obj) {
        servicios_mantenimiento(
          id_mantenimiento = id,
            id_gps = idGps,
            empresa = empresa,
            cita = cita,
            placa = placa,
            economico = economico,
            motivo = motivo,
            tecnico = tecnico,
            comentario = comentario,
            fechaAsign = creacion
        )
    }
}