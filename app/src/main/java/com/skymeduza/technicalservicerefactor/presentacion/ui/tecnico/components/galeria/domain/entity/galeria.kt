package com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.components.galeria.domain.entity

import com.skymeduza.technicalservicerefactor.data.db.base.BaseEntityImpl
import io.objectbox.annotation.Entity

@Entity
data class tb_galeria(
    var id_mantenimiento: Long = 0,
    var valor_enviado: Int = 0,
    var tipo_imagen: String = "",
    var ruta_imagen: String = "",
    var comentario_imagen: String = "",
    var imagen_ruta: String = "",
    var latitud: String = "",
    var longitud: String = "",
    var fecha: String = ""
):BaseEntityImpl()
