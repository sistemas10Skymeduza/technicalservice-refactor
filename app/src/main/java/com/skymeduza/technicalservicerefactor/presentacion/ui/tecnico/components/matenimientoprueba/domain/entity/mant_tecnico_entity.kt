package com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.components.matenimientoprueba.domain.entity

import com.skymeduza.technicalservicerefactor.data.db.base.BaseEntityImpl
import io.objectbox.annotation.Entity

@Entity
data class tb_mant_tecnico_entity(
    var id_mantenimiento: Long = 0,
    var empresa: String = "",
    var economico: String = "",
    var placa: String = "",
    var motivo: String = "",
    var comentario: String = "",
    var inicio: String = "",
    var estatus_unidad: String = "",
    var no_disponible: String = "",
    var ext_comentario: String = "",
    var ext_imagenid: String = "",
    var int_comentario: String = "",
    var int_imagenid: String = "",
    var testigos_comentario: String = "",
    var testigos_imagenid: String = "",
    var serviciocomentario: String = "",
    var servicio_imagenid: String = "",
    var cables_danados: Boolean = false,
    var sensor_puerta: Boolean = false,
    var c_canbus: Boolean = false,
    var botones_panico_comentarios: String = "",
    var destino: Boolean = false,
    var central_pyp: Boolean = false,
    var mensaje_ok: Boolean = false,
    var comentario_paro: String = "",
    var imagen_paro: String = "",
    var apagatablero: Boolean = false,
    var realizado_por: String = "",
    var otras_pruebas: String = "",
    var horaFin: String = "",
    var demora: String = "",
    var cancelacion: String = "",
    var comentario_cancelacion: String = "",
    var estatus_finalizado: Boolean = false,
    var estatus_error: Boolean = false
):BaseEntityImpl()
