package com.skymeduza.technicalservicerefactor.presentacion.base.preference

import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey


object Settings {
    val LOGIN_SUCCESSFUL = intPreferencesKey("LOGIN_SUCCESSFUL")
    val USER_ID = stringPreferencesKey("USER_ID")
    val NAME_COMPLETE = stringPreferencesKey("NAME_COMPLETE")
    val PERFIL = stringPreferencesKey("PERFIL")
}