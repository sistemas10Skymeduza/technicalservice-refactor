package com.skymeduza.technicalservicerefactor.presentacion.ui.login.data.dto

import com.google.gson.annotations.SerializedName

data class LoginDataResponse(
    @SerializedName("id") var id: String,
    @SerializedName("nombre") var nombre: String,
    @SerializedName("aPaterno") var apellido_paterno: String,
    @SerializedName("aMaterno") var apellido_materno: String,
    @SerializedName("perfil") var perfil: String
)
