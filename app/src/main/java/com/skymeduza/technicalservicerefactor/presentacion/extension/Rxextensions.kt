package com.skymeduza.technicalservicerefactor.presentacion.extension

import io.reactivex.rxjava3.core.Observable
import java.util.concurrent.TimeUnit

fun <T : Any> Observable<T>.delayEach(interval: Long, timeUnit: TimeUnit): Observable<T> =
    Observable.zip(
        this,
        Observable.interval(interval, timeUnit),
        { item, _ -> item }
    )