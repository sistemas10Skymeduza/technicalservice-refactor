package com.skymeduza.technicalservicerefactor.presentacion.ui.administrador.components.cancelados.domain.entity

import com.skymeduza.technicalservicerefactor.data.db.base.BaseEntityImpl
import io.objectbox.annotation.Entity

@Entity
data class tb_servicio_mantenimientos_cancelados(
    var id_mantenimiento: Long = 0,
    var empresa: String = "",
    var cita: String = "",
    var placa: String = "",
    var economico: String = "",
    var motivo: String = "",
    var tecnico: String = "",
    var hora_inicio: String = "",
    var hora_cancelacion: String = "",
    var tipo_cancelacion: String = "",
    var motivo_cancelacion: String = "",
    var comentario_cancelacion: String = ""
): BaseEntityImpl()
