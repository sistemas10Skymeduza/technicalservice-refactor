package com.skymeduza.technicalservicerefactor.presentacion.datastore

import android.content.Context
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.preferencesDataStore
import com.skymeduza.technicalservicerefactor.presentacion.base.preference.Settings
import com.skymeduza.technicalservicerefactor.presentacion.base.util.Constants
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject
import javax.inject.Singleton

private val Context.dataStore by preferencesDataStore("settings")

@Singleton
class DataStoreManager @Inject constructor(@ApplicationContext appContext: Context) {
    private val settingsDataStore = appContext.dataStore

    suspend fun setLoginMode(loginMode: Int) = settingsDataStore.edit { settings ->
        settings[Settings.LOGIN_SUCCESSFUL] = loginMode
    }
    suspend fun setUserId(userId: String) = settingsDataStore.edit { settings ->
        settings[Settings.USER_ID] = userId
    }
    suspend fun setPerfil(perfil: String) = settingsDataStore.edit { settings ->
        settings[Settings.PERFIL] = perfil
    }

    val loginMode: Flow<Int> = settingsDataStore.data.map { preferences ->
        preferences[Settings.LOGIN_SUCCESSFUL] ?: Constants.IS_NOT_LOGIN
    }
    val userId: Flow<String> = settingsDataStore.data.map { preferences ->
        preferences[Settings.USER_ID] ?: Constants.EMPTY_STRING
    }
    val perfil: Flow<String> = settingsDataStore.data.map { preferences ->
        preferences[Settings.PERFIL] ?: Constants.EMPTY_STRING
    }

}