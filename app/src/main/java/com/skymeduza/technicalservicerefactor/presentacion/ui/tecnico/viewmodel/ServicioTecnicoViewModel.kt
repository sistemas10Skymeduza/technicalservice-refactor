package com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.viewmodel

import com.skymeduza.technicalservicerefactor.presentacion.base.viewmodel.BaseViewModel
import com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.data.repository.ServicioMantenimientoDao
import com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.domain.entity.servicios_mantenimiento_
import com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.domain.usecase.GetServicioTecnicoParams
import com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.domain.usecase.ServiciosTecnicoSaveUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import io.objectbox.android.ObjectBoxLiveData
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.Disposable
import kotlinx.coroutines.flow.observeOn
import javax.inject.Inject

@HiltViewModel
class ServicioTecnicoViewModel  @Inject constructor(
    private val serviciosTecnicoSaveUseCase: ServiciosTecnicoSaveUseCase,
    private val servicioMantenimientoDao: ServicioMantenimientoDao
) : BaseViewModel() {

    fun fetchServicios(id_user: String): Disposable =
        serviciosTecnicoSaveUseCase(GetServicioTecnicoParams(idUser = id_user))
            .observeOn(AndroidSchedulers.mainThread())
            .autoDispose(this)
            .subscribe()

    fun getServicios(id_user: String) =
        ObjectBoxLiveData(servicioMantenimientoDao.getQuery().notNull(servicios_mantenimiento_.__ID_PROPERTY).build())
}