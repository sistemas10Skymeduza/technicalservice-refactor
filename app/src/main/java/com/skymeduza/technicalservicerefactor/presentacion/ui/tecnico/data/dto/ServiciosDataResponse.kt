package com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.data.dto

import com.google.gson.annotations.SerializedName

class ServiciosDataResponse (
    @SerializedName("id") var id: String,
    @SerializedName("idGps") var idGps: String,
    @SerializedName("empresa") var empresa: String,
    @SerializedName("cita") var cita: String,
    @SerializedName("placa") var placa: String,
    @SerializedName("economico") var economico: String,
    @SerializedName("motivo") var motivo: String,
    @SerializedName("tecnico") var tecnico: String,
    @SerializedName("comentario") var comentario: String,
    @SerializedName("creacion") var creacion: String
)