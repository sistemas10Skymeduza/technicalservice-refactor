package com.skymeduza.technicalservicerefactor.presentacion.ui.administrador.domain.entity.catalogo

import com.skymeduza.technicalservicerefactor.data.db.base.BaseEntityImpl
import io.objectbox.annotation.Entity

@Entity
data class tb_servicio_catalogo_technicians(
    var id_technicians: Long = 0,
    var nombre: String = ""
): BaseEntityImpl()
