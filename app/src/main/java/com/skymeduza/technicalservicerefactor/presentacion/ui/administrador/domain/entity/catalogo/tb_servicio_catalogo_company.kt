package com.skymeduza.technicalservicerefactor.presentacion.ui.administrador.domain.entity.catalogo

import com.skymeduza.technicalservicerefactor.data.db.base.BaseEntityImpl
import io.objectbox.annotation.Entity

@Entity
data class tb_servicio_catalogo_company(
    var id_company: Long = 0,
    var company: String = ""
): BaseEntityImpl()
