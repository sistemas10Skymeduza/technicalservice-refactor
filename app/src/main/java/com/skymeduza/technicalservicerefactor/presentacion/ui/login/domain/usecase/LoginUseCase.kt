package com.skymeduza.technicalservicerefactor.presentacion.ui.login.domain.usecase

import com.skymeduza.technicalservicerefactor.data.http.base.NetworkResponse
import com.skymeduza.technicalservicerefactor.domain.base.Failure
import com.skymeduza.technicalservicerefactor.domain.base.usecase.SingleUseCase

import com.skymeduza.technicalservicerefactor.domain.extension.allowReads
import com.skymeduza.technicalservicerefactor.domain.extension.allowWrites
import com.skymeduza.technicalservicerefactor.presentacion.ui.login.data.repository.UserDao
import com.skymeduza.technicalservicerefactor.presentacion.ui.login.data.service.LoginAPI
import com.skymeduza.technicalservicerefactor.presentacion.ui.login.domain.entity.tb_usuario
import com.skymeduza.technicalservicerefactor.presentacion.ui.login.domain.entity.tb_usuario_
import com.skymeduza.technicalservicerefactor.presentacion.ui.login.domain.entity.tb_usuario_.usuario
import com.skymeduza.technicalservicerefactor.presentacion.ui.login.domain.mapper.LoginMapper
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class LoginUseCase @Inject constructor(
    private val loginAPI: LoginAPI,
    private val userDAO: UserDao
) : SingleUseCase<LoginResult, LoginUseCaseParams> {

    override fun invoke(params: LoginUseCaseParams): Single<LoginResult> =
        loginAPI.login(usuario = params.user_name, password = params.password, dispositivo = "", version_android = 0.0, imei = "")
            .subscribeOn(Schedulers.io())
            .map { response ->
                val result = LoginResult()
                when (response) {
                    is NetworkResponse.Success -> {
                        result.is_login = response.body.success
                        if(response.body.success == 1) {
                            result.user = LoginMapper().mapLeftToRight(response.body)
                            result.message = "Inicio Sesión"
                            return@map(saveOnDB(result))
                        } else {
                            result.failure = Failure.Unknown("Datos Incorrectos")
                        }
                    }
                    is NetworkResponse.ApiError -> {
                        result.failure = Failure.Api(response.body?.message)
                    }
                    is NetworkResponse.NetworkError -> {
                        result.failure = Failure.NoInternet(response.error.message)
                    }
                    else -> {
                        result.failure = Failure.Unknown("Error desconocido")
                    }
                }
                return@map(result)
            }.onErrorReturn { throwable ->
                return@onErrorReturn(LoginResult())
            }

    private fun saveOnDB(result: LoginResult): LoginResult
    {
        var exist: tb_usuario? = null

        allowReads {
            exist = userDAO.getQuery().equal(tb_usuario_.usuario, result.user.usuario).build().findUnique()
        }

        if(exist != null)
            return result

        allowWrites {
            userDAO.saveOrUpdate(result.user)
        }

        return result
    }
}


class LoginUseCaseParams(
    val user_name: String,
    val password: String,
)

class LoginResult(
    //0 - No Session, 1 - Session Exist
    var is_login: Int = 0,
    var user: tb_usuario = tb_usuario(),
    var message: String = "",
    var failure: Failure = Failure.Unknown()
)