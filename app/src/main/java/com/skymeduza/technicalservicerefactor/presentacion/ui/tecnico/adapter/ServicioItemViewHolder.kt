package com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.adapter

import androidx.recyclerview.widget.RecyclerView
import com.skymeduza.technicalservicerefactor.databinding.ItemMantenimientosTecnicoBinding
import com.skymeduza.technicalservicerefactor.presentacion.extension.setOnReactiveClickListener
import com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.domain.entity.ui.ServicioUI

class ServicioItemViewHolder (val itemBinding: ItemMantenimientosTecnicoBinding) :
    RecyclerView.ViewHolder(itemBinding.root){

    fun bind(servicioUI: ServicioUI) = with(itemView) {
        itemBinding.tvFechaT.transitionName = servicioUI.fecha
        itemBinding.tvPlacaT.text = servicioUI.placa
        itemBinding.tvEconomicoT.text = servicioUI.economico
        itemBinding.tvNombreTecncoT.text = servicioUI.nombreTecnico
        itemBinding.tvFechaAsign.text = servicioUI.fechaAsign
        itemBinding.tvDetallesT.text = servicioUI.detalles
        itemBinding.tvComentariosT.text = servicioUI.comentarios
        itemBinding.lyMantenimientoTecnico.setOnReactiveClickListener {

        }

    }


}