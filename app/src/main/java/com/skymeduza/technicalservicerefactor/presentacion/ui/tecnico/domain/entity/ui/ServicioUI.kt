package com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.domain.entity.ui

import android.os.Parcelable
import com.skymeduza.technicalservicerefactor.presentacion.base.adapter.RecyclerItem
import kotlinx.parcelize.Parcelize

@Parcelize
data class ServicioUI(
    override val id: Int?,
    val empresa : String,
    val fecha : String,
    val placa : String,
    val economico : String,
    val nombreTecnico : String,
    val fechaAsign: String,
    val detalles : String,
    val comentarios: String

) : RecyclerItem, Parcelable
