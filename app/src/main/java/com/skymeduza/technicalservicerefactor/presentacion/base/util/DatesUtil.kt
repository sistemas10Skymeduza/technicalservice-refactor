package com.skymeduza.technicalservicerefactor.presentacion.base.util

import java.text.SimpleDateFormat
import java.util.*

class DatesUtil {

    fun getDateToString(date: Date): String =
        SimpleDateFormat(format, Locale.US).format(date)

    companion object {
        private const val format: String = "yyyy-MM-dd HH:mm:ss"
    }
}