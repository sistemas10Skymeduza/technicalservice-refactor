package com.skymeduza.technicalservicerefactor.presentacion.ui.login.data.service

import com.skymeduza.technicalservicerefactor.data.http.base.BaseApiService
import com.skymeduza.technicalservicerefactor.presentacion.ui.login.data.dto.LoginResponse
import com.skymeduza.technicalservicerefactor.data.http.base.GenericNetworkResponse
import io.reactivex.rxjava3.core.Single
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface LoginAPI : BaseApiService {
    @GET("login?")
    fun login(@Query("user") usuario:String,
              @Query("pass") password:String,
              @Query("disp") dispositivo:String,
              @Query("androidVersion") version_android:Double,
              @Query("imei") imei:String)  : Single<GenericNetworkResponse<LoginResponse>>

}