package com.skymeduza.technicalservicerefactor.presentacion.ui.administrador.components.finalizados.fragments.domain.entity

import com.skymeduza.technicalservicerefactor.data.db.base.BaseEntityImpl
import io.objectbox.annotation.Entity

@Entity
data class tb_servicio_mantenimientos_finalizados(
    var id_mantenimiento: Long = 0,
    var empresa: String = "",
    var cita: String = "",
    var placa: String = "",
    var economico: String = "",
    var motivo: String = "",
    var tecnico: String = "",
    var hora_inicio: String = "",
    var hora_finalizado: String = "",
    var tipo_finalizado: String = "",
    var motivo_finalizado: String = "",
    var comentario_finalizado: String = ""
): BaseEntityImpl()
