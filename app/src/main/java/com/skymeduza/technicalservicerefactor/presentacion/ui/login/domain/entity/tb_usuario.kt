package com.skymeduza.technicalservicerefactor.presentacion.ui.login.domain.entity

import com.skymeduza.technicalservicerefactor.data.db.base.BaseEntityImpl
import io.objectbox.annotation.Entity

@Entity
data class tb_usuario(
    var id_usuario: String = "",
    var usuario: String = "",
    var password: String = "",
    var nombre_usuario: String = "",
    var apellido_paterno: String = "",
    var apellido_materno: String = "",
    var perfil: String = ""
):BaseEntityImpl()
