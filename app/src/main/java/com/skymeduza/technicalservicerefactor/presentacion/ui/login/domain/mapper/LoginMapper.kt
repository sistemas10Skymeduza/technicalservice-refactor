package com.skymeduza.technicalservicerefactor.presentacion.ui.login.domain.mapper

import com.skymeduza.technicalservicerefactor.domain.base.mapper.Mapper
import com.skymeduza.technicalservicerefactor.presentacion.ui.login.data.dto.LoginResponse
import com.skymeduza.technicalservicerefactor.presentacion.ui.login.domain.entity.tb_usuario

class LoginMapper : Mapper<LoginResponse, tb_usuario> {
    override fun mapLeftToRight(obj: LoginResponse): tb_usuario = with(obj) {
        tb_usuario(
            id_usuario = data.id,
            nombre_usuario = data.nombre,
            apellido_paterno = data.apellido_paterno,
            apellido_materno = data.apellido_materno,
            perfil = data.perfil
        )
    }
}