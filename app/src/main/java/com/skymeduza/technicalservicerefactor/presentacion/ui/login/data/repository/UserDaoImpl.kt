package com.skymeduza.technicalservicerefactor.presentacion.ui.login.data.repository

import com.skymeduza.technicalservicerefactor.data.db.repository.BaseRepositoryImpl
import com.skymeduza.technicalservicerefactor.presentacion.ui.login.domain.entity.tb_usuario
import io.objectbox.BoxStore

class UserDaoImpl (boxStore: BoxStore) : BaseRepositoryImpl<tb_usuario>(boxStore, tb_usuario::class.java), UserDao