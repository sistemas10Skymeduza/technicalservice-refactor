package com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.domain.usecase

import com.skymeduza.technicalservicerefactor.data.http.base.NetworkResponse
import com.skymeduza.technicalservicerefactor.domain.base.Result
import com.skymeduza.technicalservicerefactor.domain.base.usecase.GeneralUseCase
import com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.data.repository.ServicioMantenimientoDao
import com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.data.service.ServiciosTecAPI
import com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.domain.entity.ui.ServicioUI
import com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.domain.mapper.ServiciosMapper
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class ServiciosTecnicoSaveUseCase @Inject constructor(
    private val serviciosDao: ServicioMantenimientoDao,
    private val serviciosTecAPI: ServiciosTecAPI
) : GeneralUseCase<Flow<Result<ServicioUI, Any>>, GetServicioTecnicoParams> {
    override fun invoke(params: GetServicioTecnicoParams): Flow<Result<ServicioUI, Any>> = flow {
        val respuestaServicio = serviciosTecAPI.getMantenimientos(params.idUser)
        when(respuestaServicio){
            is NetworkResponse.Success -> {
                val body = respuestaServicio.body
                if (body.success == 1){
                    serviciosDao.saveOrUpdateAll(body.data.map {
                        ServiciosMapper().mapLeftToRight(it)
                    })
                }
            }
        }
    }
}

@JvmInline
value class GetServicioTecnicoParams(val idUser : String)