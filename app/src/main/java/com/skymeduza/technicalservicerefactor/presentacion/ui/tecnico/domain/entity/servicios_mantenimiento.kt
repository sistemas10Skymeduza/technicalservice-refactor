package com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.domain.entity

import com.skymeduza.technicalservicerefactor.data.db.base.BaseEntityImpl
import io.objectbox.annotation.Entity

@Entity()
data class servicios_mantenimiento (
    var id_mantenimiento:String = "",
    var id_gps:String = "",
    var empresa: String = "",
    var cita: String = "",
    var placa: String ="",
    var economico: String = "",
    var motivo: String = "",
    var tecnico: String ="",
    var comentario: String ="",
    var fechaAsign: String =""
): BaseEntityImpl()