package com.skymeduza.technicalservicerefactor.presentacion.base.util.conectivity

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ConnectivityManagerImpl @Inject constructor(
    application: Application
) : ConnectivityManager {
    private val connectionLiveData = ConnectionLiveData(application)

    private val networkObserver = Observer<Boolean> { isConnected -> isNetworkAvailable.value = isConnected }

    override val isNetworkAvailable = MutableLiveData<Boolean>()

    override fun registerConnectionObserver() {
        connectionLiveData.observeForever(networkObserver)
    }

    override fun unregisterConnectionObserver() {
        connectionLiveData.removeObserver(networkObserver)
    }
}