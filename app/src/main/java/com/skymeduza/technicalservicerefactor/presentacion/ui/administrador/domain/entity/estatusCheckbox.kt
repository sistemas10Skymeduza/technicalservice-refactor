package com.skymeduza.technicalservicerefactor.presentacion.ui.administrador.domain.entity

import com.skymeduza.technicalservicerefactor.data.db.base.BaseEntityImpl
import io.objectbox.annotation.Entity

@Entity
data class estatusCheckbox(
    var id_mantenimiento: Long = 0,
    var name_checkbox: String = "",
    var estatus: String = ""
):BaseEntityImpl()
