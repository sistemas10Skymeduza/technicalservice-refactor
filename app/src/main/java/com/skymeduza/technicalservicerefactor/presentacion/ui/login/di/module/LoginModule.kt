package com.skymeduza.technicalservicerefactor.presentacion.ui.login.di.module

import com.skymeduza.technicalservicerefactor.presentacion.ui.login.data.repository.UserDao
import com.skymeduza.technicalservicerefactor.presentacion.ui.login.data.repository.UserDaoImpl
import com.skymeduza.technicalservicerefactor.presentacion.ui.login.data.service.LoginAPI
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import io.objectbox.BoxStore
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object LoginModule {

    @Provides
    @Singleton
    fun userRepository(boxStore: BoxStore): UserDao = UserDaoImpl(boxStore)

    @Provides
    @Singleton
    fun loginService(retrofit: Retrofit): LoginAPI = retrofit.create(LoginAPI::class.java)
}