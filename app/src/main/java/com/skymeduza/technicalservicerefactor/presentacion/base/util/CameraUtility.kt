package com.skymeduza.technicalservicerefactor.presentacion.base.util

import android.Manifest
import android.content.Context
import android.os.Build
import dagger.hilt.android.qualifiers.ApplicationContext
import pub.devrel.easypermissions.EasyPermissions
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CameraUtility @Inject constructor(@ApplicationContext private val appContext: Context) {
    fun hasCameraPermissions() =
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.Q) {
            EasyPermissions.hasPermissions(
                appContext,
                Manifest.permission.CAMERA
            )
        } else {
            EasyPermissions.hasPermissions(
                appContext,
                Manifest.permission.CAMERA
            )
        }
}