package com.skymeduza.technicalservicerefactor.presentacion.base.util

import android.content.Context
import android.widget.Toast
import androidx.annotation.StringRes
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CustomToast @Inject constructor(@ApplicationContext private val appContext: Context) {
    fun showMessage(msg: String, duration: Int = Constants.TOAST_DURATION) =
        Toast.makeText(
            appContext.applicationContext,
            msg,
            duration
        ).show()
    fun showMessage(@StringRes textRes: Int, duration: Int = Constants.TOAST_DURATION) =
        Toast.makeText(
            appContext.applicationContext,
            textRes,
            duration
        ).show()
}