package com.skymeduza.technicalservicerefactor.presentacion.ui.login.data.repository

import com.skymeduza.technicalservicerefactor.data.db.repository.interfaces.IBaseRepository
import com.skymeduza.technicalservicerefactor.presentacion.ui.login.domain.entity.tb_usuario

interface UserDao : IBaseRepository<tb_usuario>