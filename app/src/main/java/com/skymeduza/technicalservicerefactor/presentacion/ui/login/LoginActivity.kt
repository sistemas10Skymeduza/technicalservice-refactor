package com.skymeduza.technicalservicerefactor.presentacion.ui.login

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View.inflate
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import com.microsoft.appcenter.analytics.Analytics
import com.skymeduza.technicalservicerefactor.R
import com.skymeduza.technicalservicerefactor.databinding.ActivityLoginBinding
import com.skymeduza.technicalservicerefactor.domain.extension.allowReads
import com.skymeduza.technicalservicerefactor.domain.extension.allowWrites
import com.skymeduza.technicalservicerefactor.presentacion.base.util.AppUtils
import com.skymeduza.technicalservicerefactor.presentacion.base.util.Constants
import com.skymeduza.technicalservicerefactor.presentacion.base.util.CustomToast
import com.skymeduza.technicalservicerefactor.presentacion.datastore.DataStoreManager
import com.skymeduza.technicalservicerefactor.presentacion.extension.setOnReactiveClickListener
import com.skymeduza.technicalservicerefactor.presentacion.extension.viewBinding
import com.skymeduza.technicalservicerefactor.presentacion.ui.administrador.ServiciosActivity
import com.skymeduza.technicalservicerefactor.presentacion.ui.login.domain.usecase.LoginResult
import com.skymeduza.technicalservicerefactor.presentacion.ui.login.viewmodel.LoginViewModel
import com.skymeduza.technicalservicerefactor.presentacion.ui.main.MainActivity
import com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.ServiciosTecnicoActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Job
import timber.log.Timber
import java.lang.Math.log
import javax.inject.Inject

@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {
    private val TAG = "LoginActivity"

    private val binding by viewBinding(ActivityLoginBinding::inflate)
    private val loginViewModel: LoginViewModel by viewModels()
    private var uiStateJob: Job? = null

    @Inject
    lateinit var dataStoreManager: DataStoreManager
    @Inject
    lateinit var customToast: CustomToast
    @Inject
    lateinit var appUtils: AppUtils

    private lateinit var clipboard: ClipboardManager
    private lateinit var claveCompuesta: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        Analytics.trackEvent(TAG)

        clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager

        setupListeners()

        Timber.e("${appUtils.getDeviceId()}_${appUtils.getDeviceName()}")
        claveCompuesta = "${appUtils.getDeviceId()}_${appUtils.getDeviceName()}"
    }

    private fun setupListeners() {
        binding.btnIniciar.setOnReactiveClickListener {
            //Analytics.trackEvent("Login Click")
            allowReads {
                loginViewModel.onIniciarSession(binding.edtUsuario.text.toString(), binding.edtContraseA.text.toString()) { loginResult ->
                    if(loginResult.is_login == 1) {
                        customToast.showMessage(loginResult.message)
                        savePreferences(loginResult)
                    } else {
                       Timber.e("Dentro de else ${loginResult.message}")
                        loginResult.failure.msg?.let { failure -> customToast.showMessage(failure) }
                    }
                }
            }
        }

    }

    override fun onStop() {
        uiStateJob?.cancel()
        super.onStop()
    }

    override fun onDestroy() {
        if (isTaskRoot && isFinishing) {
            finishAfterTransition()
        }
        super.onDestroy()
    }

    private fun savePreferences(loginResult: LoginResult) {
        allowWrites {
            uiStateJob = lifecycleScope.launchWhenCreated {
                dataStoreManager.setUserId(loginResult.user.id_usuario)
                dataStoreManager.setLoginMode(loginResult.is_login)
                navigateTo(loginResult.user.perfil)
            }
        }
    }

    private fun navigateTo(perfil: String){
        when(perfil) {
            "Tecnico" -> {
                val intent = Intent(this, ServiciosTecnicoActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }
            else -> {
                val intent = Intent(this, ServiciosActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }
        }
    }
}