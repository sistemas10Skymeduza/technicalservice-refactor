package com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.components.infogps.domain.entity

import com.skymeduza.technicalservicerefactor.data.db.base.BaseEntityImpl
import io.objectbox.annotation.Entity

@Entity
data class infoGps(
    var tipo: String = "",
    var id_gps: Int = 0,
    var telefono: String = "",
    var ubicacion: String = "",
    var mantenimiento_id: Int = 0,
    var ruta_imagenes: String = ""
):BaseEntityImpl()
