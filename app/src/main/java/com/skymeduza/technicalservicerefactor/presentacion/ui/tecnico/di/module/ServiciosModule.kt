package com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.di.module

import com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.data.service.ServiciosTecAPI
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ServiciosModule {

    @Provides
    @Singleton
    fun ServiciosService(retrofit: Retrofit): ServiciosTecAPI = retrofit.create(ServiciosTecAPI::class.java)

}