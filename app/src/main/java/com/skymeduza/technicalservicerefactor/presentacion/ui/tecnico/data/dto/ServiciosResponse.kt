package com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.data.dto

import com.google.gson.annotations.SerializedName

class ServiciosResponse (
    @SerializedName("success") var success: Int,
    @SerializedName("data") var data: List<ServiciosDataResponse>
)