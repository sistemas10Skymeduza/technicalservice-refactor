package com.skymeduza.technicalservicerefactor.presentacion.base.util

import android.widget.Toast

object Constants {
    const val IS_LOGIN: Boolean = true
    const val IS_NOT_LOGIN: Int = 0
    const val EMPTY_INTEGER: Int = 0
    const val EMPTY_STRING: String = ""
    const val TOAST_DURATION = Toast.LENGTH_SHORT
    const val STRING_FALSE = "false"
    const val STRING_TRUE = "true"
    const val REQUEST_CODE_CAMERA_PERMISSION = 101
    const val FILENAME_FORMAT = "yyyy-MM-dd-HH-mm-ss-SSS"
}