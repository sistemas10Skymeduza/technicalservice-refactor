package com.skymeduza.technicalservicerefactor.presentacion.ui.administrador.domain.entity.catalogo

import com.skymeduza.technicalservicerefactor.data.db.base.BaseEntityImpl
import io.objectbox.annotation.Entity

@Entity
data class tb_servicio_catalogo_cancel_reasons(
    var id_reason: Long = 0,
    var concepto: String = "",
    var tipo: String = "",
    var estatus: String = ""
): BaseEntityImpl()
