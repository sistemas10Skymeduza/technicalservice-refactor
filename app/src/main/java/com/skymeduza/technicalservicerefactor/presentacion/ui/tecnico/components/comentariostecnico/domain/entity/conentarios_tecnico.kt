package com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.components.comentariostecnico.domain.entity

import com.skymeduza.technicalservicerefactor.data.db.base.BaseEntityImpl
import io.objectbox.annotation.Entity

@Entity
data class tb_conentarios_tecnico(
    var id_mantenimiento: Long = 0,
    var estatus: String = "",
    var interior: String = "",
    var exterior: String = "",
    var testigos: String = "",
    var servicio: String = "",
    var botones_panico: String = "",
    var paro_motor: String = "",
    var otras_pruebas: String = ""
):BaseEntityImpl()
