package com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.skymeduza.technicalservicerefactor.R
import com.skymeduza.technicalservicerefactor.databinding.ItemMantenimientosTecnicoBinding
import com.skymeduza.technicalservicerefactor.presentacion.base.adapter.Cell
import com.skymeduza.technicalservicerefactor.presentacion.base.adapter.RecyclerItem
import com.skymeduza.technicalservicerefactor.presentacion.extension.setOnReactiveClickListener
import com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.domain.entity.ui.ServicioUI

object ServicioItemCell : Cell<RecyclerItem, ViewBinding> {
    override fun belongsTo(item: RecyclerItem?): Boolean {
        return item is ServicioUI
    }

    override fun type(): Int {
        return R.layout.activity_servicios_tecnico
    }

    override fun binding(parent: ViewGroup) : ItemMantenimientosTecnicoBinding {
        return ItemMantenimientosTecnicoBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    }

    override fun holder(parent: ViewGroup): RecyclerView.ViewHolder {
        return ServicioItemViewHolder(binding(parent))
    }

    override fun bind(
        holder: RecyclerView.ViewHolder,
        item: RecyclerItem?,
        onItemClick: ((RecyclerItem, View) -> Unit)?
    ) {
        if(holder is ServicioItemViewHolder && item is ServicioUI) {
            holder.bind(item)
            holder.itemView.setOnReactiveClickListener {
                onItemClick?.run {
                    this(item, holder.itemBinding.lyMantenimientoTecnico)
                }
            }
        }
    }
}