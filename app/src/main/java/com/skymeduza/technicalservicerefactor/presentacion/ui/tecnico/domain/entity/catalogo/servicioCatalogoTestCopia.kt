package com.skymeduza.technicalservicerefactor.presentacion.ui.tecnico.domain.entity.catalogo

import com.skymeduza.technicalservicerefactor.data.db.base.BaseEntityImpl
import io.objectbox.annotation.Entity

@Entity
data class servicioCatalogoTestCopia(
    var id_test: Long = 0,
    var test: String = "",
    var estatus: String = "",
    var id_mantenimiento: Long = 0
): BaseEntityImpl()
