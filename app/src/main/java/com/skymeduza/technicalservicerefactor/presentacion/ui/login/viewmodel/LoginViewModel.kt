package com.skymeduza.technicalservicerefactor.presentacion.ui.login.viewmodel

import autodispose2.autoDispose
import com.skymeduza.technicalservicerefactor.presentacion.base.viewmodel.BaseViewModel
import com.skymeduza.technicalservicerefactor.presentacion.ui.login.domain.usecase.LoginResult
import com.skymeduza.technicalservicerefactor.presentacion.ui.login.domain.usecase.LoginUseCase
import com.skymeduza.technicalservicerefactor.presentacion.ui.login.domain.usecase.LoginUseCaseParams
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import javax.inject.Inject

@HiltViewModel
class LoginViewModel@Inject constructor(
    private val loginUseCase: LoginUseCase
) : BaseViewModel()  {

    fun onIniciarSession(user_name: String, password: String, onResult: (next: LoginResult) -> Unit) {
        loginUseCase(LoginUseCaseParams(user_name, password ))
            .observeOn(AndroidSchedulers.mainThread())
            .autoDispose(this)
            .subscribe { res ->
                onResult(res)
            }
    }
}