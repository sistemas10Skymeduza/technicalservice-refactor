package com.skymeduza.technicalservicerefactor.data.http.base

typealias GenericNetworkResponse<S> = NetworkResponse<S, ErrorBody>

interface BaseApiService