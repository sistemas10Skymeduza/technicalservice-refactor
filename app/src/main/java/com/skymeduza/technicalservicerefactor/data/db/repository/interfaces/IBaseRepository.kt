package com.skymeduza.technicalservicerefactor.data.db.repository.interfaces

import io.objectbox.android.ObjectBoxLiveData
import io.objectbox.query.QueryBuilder

interface IBaseRepository<T> {
    /**
     * Get all elements
     */
    fun getAll(): ObjectBoxLiveData<T>

    /**
     * Find element by Id
     */
    fun findById(id: Long): T

    /**
     * Generic Search
     */
    fun getQuery(): QueryBuilder<T>

    /**
     * Save or Update item
     */
    fun saveOrUpdate(entity: T): T

    /**
     * Save or update Items
     */
    fun saveOrUpdateAll(entities: Collection<T>)

    /**
     * Delete all items
     */
    fun deleteAll()

    /**
     * Delete item by id
     */
    fun deleteById(id: Long): Boolean
}