package com.skymeduza.technicalservicerefactor.data.http.dto

import com.google.gson.annotations.SerializedName

data class GeneralResponse<T>(
    @SerializedName("success") var Success : Int = 0,
    @SerializedName("data") var Data : T,
    @SerializedName("message") var Message : String = ""
)
