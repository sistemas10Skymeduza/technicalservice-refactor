package com.skymeduza.technicalservicerefactor.data.db.base.interfaces

interface IBaseEntity {
    val id: Long
}