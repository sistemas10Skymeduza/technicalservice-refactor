package com.skymeduza.technicalservicerefactor.data.http.base

data class ErrorBody(
    val statusCode: Int,
    val error: String,
    val message: String
)