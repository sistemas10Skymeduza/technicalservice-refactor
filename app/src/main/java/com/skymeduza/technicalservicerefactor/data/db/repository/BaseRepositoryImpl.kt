package com.skymeduza.technicalservicerefactor.data.db.repository

import com.skymeduza.technicalservicerefactor.data.db.base.interfaces.IBaseEntity
import com.skymeduza.technicalservicerefactor.data.db.repository.interfaces.IBaseRepository
import io.objectbox.Box
import io.objectbox.BoxStore
import io.objectbox.android.ObjectBoxLiveData
import io.objectbox.query.QueryBuilder

abstract class BaseRepositoryImpl<T>(
    val boxStore: BoxStore,
    tClass: Class<T>
): IBaseRepository<T> where T : IBaseEntity {

    private val tBox: Box<T> = boxStore.boxFor(tClass)

    override fun getAll(): ObjectBoxLiveData<T> = ObjectBoxLiveData(tBox.query().build())

    override fun findById(id: Long): T = tBox.get(id)

    override fun getQuery(): QueryBuilder<T> = tBox.query()

    override fun saveOrUpdate(entity: T): T {
        tBox.put(entity)
        return entity
    }

    override fun saveOrUpdateAll(entities: Collection<T>) = tBox.put(entities)

    override fun deleteAll() = tBox.removeAll()

    override fun deleteById(id: Long): Boolean = tBox.remove(id)
}