package com.skymeduza.technicalservicerefactor.data.db.base

import com.skymeduza.technicalservicerefactor.data.db.base.interfaces.IBaseEntity
import io.objectbox.annotation.BaseEntity
import io.objectbox.annotation.Id

@BaseEntity
abstract class BaseEntityImpl : IBaseEntity {
    @Id
    final override var id: Long = 0

    constructor()

    constructor(id: Long) {
        this.id = id
    }
}