package com.skymeduza.technicalservicerefactor.data.db

import android.content.Context
import com.skymeduza.technicalservicerefactor.MyObjectBox
import dagger.hilt.android.qualifiers.ApplicationContext
import io.objectbox.BoxStore
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ObjectBox @Inject constructor(
    @ApplicationContext appContext: Context
) {
    val boxStore: BoxStore = MyObjectBox
        .builder()
        .androidContext(appContext.applicationContext)
        .build()
}