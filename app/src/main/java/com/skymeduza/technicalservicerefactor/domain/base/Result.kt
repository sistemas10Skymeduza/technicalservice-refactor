package com.skymeduza.technicalservicerefactor.domain.base

/**
 * Represents the result of use case
 *
 * @param T success proccess
 * @param U error in the use case
 */
sealed class Result<out T : Any, out U : Any> {

    /**
     * The proccess complete
     */
    data class Success<T : Any>(val data: T) : Result<T, Nothing>()

    /**
     * An error in the proccess in use case
     */
    data class Error<U : Any>(val error: U) : Result<Nothing, U>()

    /**
     * An Exception cause by Android
     */
    data class Exception(val cause: Cause) : Result<Nothing, Nothing>() {
        interface Cause
    }
}