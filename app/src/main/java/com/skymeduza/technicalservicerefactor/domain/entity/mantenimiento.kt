package com.skymeduza.technicalservicerefactor.domain.entity

import com.skymeduza.technicalservicerefactor.data.db.base.BaseEntityImpl
import io.objectbox.annotation.Entity

@Entity
data class mantenimiento(
    var fecha: String = "",
    var nombre: String = "",
    var tipoMantenimiento: String = ""
):BaseEntityImpl()
