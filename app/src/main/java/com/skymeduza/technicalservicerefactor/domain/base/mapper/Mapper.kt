package com.skymeduza.technicalservicerefactor.domain.base.mapper

/**
 * Interfaz para convertir un objeto a otro
 * Ejemplo: Entidad a DTO
 * @author Jonatan Tlilayatzi Cruz
 */
interface Mapper<in LeftObject, out RightObject> {
    /**
     * Convertir Objetos
     */
    fun mapLeftToRight(obj: LeftObject): RightObject
}