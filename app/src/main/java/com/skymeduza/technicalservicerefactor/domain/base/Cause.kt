package com.skymeduza.technicalservicerefactor.domain.base

object NoConnection : Result.Exception.Cause

object InsufficientSpace : Result.Exception.Cause

object Unknown : Result.Exception.Cause